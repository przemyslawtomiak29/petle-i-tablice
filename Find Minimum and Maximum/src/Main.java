public class Main {
    public static void main(String[] args) {
        int[] array = {9, 3, 1, 2, 4, 6};

        int smallestNumber = array[0];
        int largestNumber = array[0];
        int smallestIndex = 0;
        int largestIndex = 0;

        for (int i = 1; i < array.length; i++) {
            if (array[i] < smallestNumber) {
                smallestNumber = array[i];
                smallestIndex = i;
            }
            if (array[i] > largestNumber) {
                largestNumber = array[i];
                largestIndex = i;
            }
        }
        System.out.println("Smallest number: " + smallestNumber);
        System.out.println("Smallest number index: " + smallestIndex);
        System.out.println("Largest number: " + largestNumber);
        System.out.println("Largest number index: " + largestIndex);
    }
}