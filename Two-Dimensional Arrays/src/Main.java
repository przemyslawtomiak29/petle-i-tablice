import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int rows = 4;
        int columns = 5;

        int[][] array = new int[rows][columns];

        Random random = new Random();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = random.nextInt(100);
            }
        }
        for (int i = 0; i < rows; i++) {
            int rowSum = 0;
            for (int j = 0; j < columns; j++) {
                rowSum =rowSum + array[i][j];
            }
            System.out.println("Sum of row " + i + ": " + rowSum);
        }

        for (int j = 0; j < columns; j++) {
            int columnSum = 0;
            for (int i = 0; i < rows; i++) {
                columnSum =columnSum + array[i][j];
            }
            System.out.println("Sum of column " + j + ": " + columnSum);
        }
    }
}