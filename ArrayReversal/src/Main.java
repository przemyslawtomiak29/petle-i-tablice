public class Main {
    public static void main(String[] args) {

            int[] array = new int[]{2, 4, 6, 8, 10};
            System.out.print("Array: ");

            for(int a = 0; a < array.length; a++)
            {
                System.out.print(array[a] + " ");
            }
            System.out.println();

            System.out.print("Reverse array: ");
            for(int a = array.length - 1; a >= 0; a--)
            {
                System.out.print(array[a] + " ");
            }
        }
    }