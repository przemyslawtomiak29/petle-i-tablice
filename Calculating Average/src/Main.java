import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        double[] array = new double[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextDouble();
        }

        double sum = 0.0;
        for (int i = 0; i < size; i++) {
            sum = sum + array[i];
        }

        double average = sum / size;
        System.out.println("The average of the array is: " + average);
    }
}